# Default Flask
from flask import Flask, jsonify, render_template
from flask.scaffold import F
from config import DevConfig
# CROS
from flask_cors import CORS
# misc
import requests
import time
import json
import redis
import asyncio
import datetime

app = Flask(__name__, static_url_path='')
app.config.from_object(DevConfig)

CORS(app)


@app.route('/')
def default_index():
    return 'ok'


@app.route('/query')
def default_query():
    url = "https://aomp109.judicial.gov.tw/judbp/wkw/WHD1A02/QUERY.htm"
    payload = "gov=&crtnm=%E5%85%A8%E9%83%A8&court=&county=2&town=%E5%A4%A7%E7%94%B2%E5%8D%80%2C%E5%A4%A7%E5%AE%89%E5%8D%80%2C%E5%A4%A7%E8%82%9A%E5%8D%80%2C%E5%A4%A7%E9%87%8C%E5%8D%80%2C%E5%A4%A7%E9%9B%85%E5%8D%80%2C%E4%B8%AD%E5%8D%80%2C%E5%A4%AA%E5%B9%B3%E5%8D%80%2C%E5%8C%97%E5%B1%AF%E5%8D%80%2C%E5%8C%97%E5%8D%80%2C%E5%A4%96%E5%9F%94%E5%8D%80%2C%E7%9F%B3%E5%B2%A1%E5%8D%80%2C%E5%90%8E%E9%87%8C%E5%8D%80%2C%E8%A5%BF%E5%B1%AF%E5%8D%80%2C%E8%A5%BF%E5%8D%80%2C%E6%B2%99%E9%B9%BF%E5%8D%80%2C%E5%92%8C%E5%B9%B3%E5%8D%80%2C%E6%9D%B1%E5%8D%80%2C%E6%9D%B1%E5%8B%A2%E5%8D%80%2C%E5%8D%97%E5%B1%AF%E5%8D%80%2C%E5%8D%97%E5%8D%80%2C%E7%83%8F%E6%97%A5%E5%8D%80%2C%E7%A5%9E%E5%B2%A1%E5%8D%80%2C%E6%A2%A7%E6%A3%B2%E5%8D%80%2C%E6%B8%85%E6%B0%B4%E5%8D%80%2C%E6%96%B0%E7%A4%BE%E5%8D%80%2C%E6%BD%AD%E5%AD%90%E5%8D%80%2C%E9%BE%8D%E4%BA%95%E5%8D%80%2C%E8%B1%90%E5%8E%9F%E5%8D%80%2C%E9%9C%A7%E5%B3%B0%E5%8D%80&proptype=C51C52&saletype=1&keyword=&saledate1=&saledate2=&minprice1=&minprice2=&saleno=&crmyy=&crmid=&crmno=&dpt=&comm_yn=&stopitem=&sec=&rrange=&area1=&area2=&debtor=&checkyn=&emptyyn=&ttitle=&sorted_column=A.CRMYY%2C+A.CRMID%2C+A.CRMNO%2C+A.SALENO%2C+A.ROWID&sorted_type=ASC&_ORDER_BY=&pageNum=7&pageSize=1"
    # payload = "gov=&crtnm=%E5%85%A8%E9%83%A8&court=&county=&town=&proptype=C52&saletype=1&keyword=&saledate1=&saledate2=&minprice1=&minprice2=&saleno=&crmyy=&crmid=&crmno=&dpt=&comm_yn=&stopitem=&sec=&rrange=&area1=&area2=&debtor=&checkyn=&emptyyn=&ttitle=&sorted_column=A.CRMYY%2C+A.CRMID%2C+A.CRMNO%2C+A.SALENO%2C+A.ROWID&sorted_type=ASC&_ORDER_BY=&pageNum=1&pageSize=50"
    headers = {
        'Content-Type':
        'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin':
        'https://aomp109.judicial.gov.tw',
        'Cookie':
        'TS018d5348=0126e2a9defef0d512dc65ce26000739199746e7ca8cac9e87a361fcd97d8f0647da7dfc99e25d71db60fc81ad7a0e91d31ca4f71e'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    # do it later by async/await and muitple theadhold
    # use the while to get list of payload
    payloadList = []
    keyList = redisKeyGet('internal')
    totalList = []
    newCaseList = [] # store result to json later
    changeCaseList = [] # store result to json later
    totalNum = response.json()['pageInfo']['totalNum']
    limit = int(totalNum / 100) + 1
    print(f'totalNum = {totalNum}')
    print(f'limit = {limit}')
    curfinger = 0
    while curfinger < limit:
        x = curfinger + 1
        payloadList.append(
            f"gov=&crtnm=%E5%85%A8%E9%83%A8&court=&county=2&town=%E5%A4%A7%E7%94%B2%E5%8D%80%2C%E5%A4%A7%E5%AE%89%E5%8D%80%2C%E5%A4%A7%E8%82%9A%E5%8D%80%2C%E5%A4%A7%E9%87%8C%E5%8D%80%2C%E5%A4%A7%E9%9B%85%E5%8D%80%2C%E4%B8%AD%E5%8D%80%2C%E5%A4%AA%E5%B9%B3%E5%8D%80%2C%E5%8C%97%E5%B1%AF%E5%8D%80%2C%E5%8C%97%E5%8D%80%2C%E5%A4%96%E5%9F%94%E5%8D%80%2C%E7%9F%B3%E5%B2%A1%E5%8D%80%2C%E5%90%8E%E9%87%8C%E5%8D%80%2C%E8%A5%BF%E5%B1%AF%E5%8D%80%2C%E8%A5%BF%E5%8D%80%2C%E6%B2%99%E9%B9%BF%E5%8D%80%2C%E5%92%8C%E5%B9%B3%E5%8D%80%2C%E6%9D%B1%E5%8D%80%2C%E6%9D%B1%E5%8B%A2%E5%8D%80%2C%E5%8D%97%E5%B1%AF%E5%8D%80%2C%E5%8D%97%E5%8D%80%2C%E7%83%8F%E6%97%A5%E5%8D%80%2C%E7%A5%9E%E5%B2%A1%E5%8D%80%2C%E6%A2%A7%E6%A3%B2%E5%8D%80%2C%E6%B8%85%E6%B0%B4%E5%8D%80%2C%E6%96%B0%E7%A4%BE%E5%8D%80%2C%E6%BD%AD%E5%AD%90%E5%8D%80%2C%E9%BE%8D%E4%BA%95%E5%8D%80%2C%E8%B1%90%E5%8E%9F%E5%8D%80%2C%E9%9C%A7%E5%B3%B0%E5%8D%80&proptype=C51C52&saletype=1&keyword=&saledate1=&saledate2=&minprice1=&minprice2=&saleno=&crmyy=&crmid=&crmno=&dpt=&comm_yn=&stopitem=&sec=&rrange=&area1=&area2=&debtor=&checkyn=&emptyyn=&ttitle=&sorted_column=A.CRMYY%2C+A.CRMID%2C+A.CRMNO%2C+A.SALENO%2C+A.ROWID&sorted_type=ASC&_ORDER_BY=&pageNum={x}&pageSize=100")
        curfinger = curfinger + 1

    new_loop = asyncio.new_event_loop()
    asyncio.set_event_loop(new_loop)
    loop = asyncio.get_event_loop()

    async def send_req(url, payload):
        headers = {
            'Content-Type':
            'application/x-www-form-urlencoded; charset=UTF-8',
            'Origin':
            'https://aomp109.judicial.gov.tw',
                'Cookie':
                'TS018d5348=0126e2a9defef0d512dc65ce26000739199746e7ca8cac9e87a361fcd97d8f0647da7dfc99e25d71db60fc81ad7a0e91d31ca4f71e'
        }
        res = await loop.run_in_executor(None, lambda: requests.request("POST", url, headers=headers, data=payload))
        r = redis.StrictRedis(host='127.0.0.1', port=6379,
                              db=0, charset="utf-8", decode_responses=True)

        for i in res.json()['data']:
            tmpKey = i['rowid'] # int
            totalList.append(str(tmpKey))
             # check key exist or not
            if str(tmpKey) in keyList:
                defaultFalse = False # check changeList has dict info or not
                defaultHashtable = {} # changeList dict
                for key, value in i.items():
                    # check the value
                    if str(value) != r.hget(tmpKey, key):
                        # save the value to dict(haskKey=tmpKey, key=value) 
                        if defaultFalse == False:
                            # first time to create the dict
                            # dict[rowId] = [(column, old value, newvalue)]
                            defaultHashtable[tmpKey] = [
                                (key, r.hget(tmpKey, key), str(value))]
                            defaultFalse = True
                        else:
                            # add tuple to list
                            tmpList = defaultHashtable[tmpKey]
                            tmpList.append((key, r.hget(tmpKey, key), str(value)))
                            # adjust dict list
                            defaultHashtable[tmpKey] = tmpList
                        # update the value to DB
                        r.hset(tmpKey, key, value)
                if defaultFalse:
                    # means need to append dict to changeCaseList
                    changeCaseList.append(defaultHashtable)
            else:
                # add to hash
                newCaseList.append(tmpKey)
                for key, value in i.items():
                    r.hset(tmpKey, key, value)


    tasks = []

    print(f'payloadList = {len(payloadList)}')

    for index ,i in enumerate(payloadList):
        print(f'hit {index} - time.sleep')
        time.sleep(10)
        print(f'end {index} - time.sleep')
        task = loop.create_task(send_req(url, i))
        tasks.append(task)
    loop.run_until_complete(asyncio.wait(tasks))

    # check db total key
    if len(keyList) > len(totalList):
        r = redis.StrictRedis(host='127.0.0.1', port=6379, db=0, charset="utf-8", decode_responses=True)
        delCaseList = list(set(keyList) - set(totalList))
        for i in delCaseList:
            r.delete(int(i))
    else:
        delCaseList = []
    

    # write the result by result such as result_date
    curTime = datetime.datetime.now()
    curDate = curTime.strftime("%Y%m%d")
 
    # save result to json file
    with open(f"results_{curDate}.json", "w", encoding='utf-8') as fp:
        json.dump([newCaseList, changeCaseList, totalList, keyList, delCaseList], fp, ensure_ascii=False)
    fp.close()

    return 'ok'

# get all keys or one key hash info


@app.route('/redis/key/get/')
@app.route('/redis/key/get/<target>')
def redisKeyGet(target='all'):
    r = redis.StrictRedis(host='127.0.0.1', port=6379,
                          db=0, charset="utf-8", decode_responses=True)
    if target == 'all':
        allKeyList = r.keys(_Key='*')
        return jsonify(allKeyList)
    elif target == 'internal':
        return r.keys(_Key='*')
    else:
        if target in r.keys(_Key='*'):
            return 'exist'
        else:
            return 'does not exist'

# get hash data by key
# example - http://127.0.0.1:5000/redis/hash/get/510637


@app.route('/redis/hash/get/<targetKey>')
def redisGet(targetKey):
    keyList = redisKeyGet('internal')
    if targetKey in keyList:
        # connect redis
        r = redis.StrictRedis(host='127.0.0.1', port=6379,
                              db=0, charset="utf-8", decode_responses=True)
        # get hash data by key
        tmpContainer = r.hgetall(targetKey)
        return jsonify(tmpContainer)
    else:
        return f'does not exist - key {targetKey}'


@app.route('/check/file')
def redisConnect():
    r = redis.StrictRedis(host='127.0.0.1', port=6379,
                          db=0, charset="utf-8", decode_responses=True)

    with open('results.json', 'r') as fobj:
        curDict = json.load(fobj)['data']
    fobj.close()

    for i in curDict:
        # i['rowid']
        for key, value in i.items():
            print(f'key = {key}')
            print(f'value = {value}')
        print('----')
    return 'ok'


@app.route('/checkColumnsType')
def checkColumnsType():
    r = redis.StrictRedis(host='127.0.0.1', port=6379,
                          db=0, charset="utf-8", decode_responses=True)
    resultDict = r.hgetall('513590')
    for k, v in resultDict.items():
        print(f'{k}: {type(v)} - {v}')
    return 'ok'


# call gov source and compare the value between response and db data
@app.route('/queryTest')
def queryTest():
    url = "https://aomp109.judicial.gov.tw/judbp/wkw/WHD1A02/QUERY.htm"
    payload = "gov=&crtnm=%E5%85%A8%E9%83%A8&court=&county=2&town=%E5%A4%A7%E7%94%B2%E5%8D%80%2C%E5%A4%A7%E5%AE%89%E5%8D%80%2C%E5%A4%A7%E8%82%9A%E5%8D%80%2C%E5%A4%A7%E9%87%8C%E5%8D%80%2C%E5%A4%A7%E9%9B%85%E5%8D%80%2C%E4%B8%AD%E5%8D%80%2C%E5%A4%AA%E5%B9%B3%E5%8D%80%2C%E5%8C%97%E5%B1%AF%E5%8D%80%2C%E5%8C%97%E5%8D%80%2C%E5%A4%96%E5%9F%94%E5%8D%80%2C%E7%9F%B3%E5%B2%A1%E5%8D%80%2C%E5%90%8E%E9%87%8C%E5%8D%80%2C%E8%A5%BF%E5%B1%AF%E5%8D%80%2C%E8%A5%BF%E5%8D%80%2C%E6%B2%99%E9%B9%BF%E5%8D%80%2C%E5%92%8C%E5%B9%B3%E5%8D%80%2C%E6%9D%B1%E5%8D%80%2C%E6%9D%B1%E5%8B%A2%E5%8D%80%2C%E5%8D%97%E5%B1%AF%E5%8D%80%2C%E5%8D%97%E5%8D%80%2C%E7%83%8F%E6%97%A5%E5%8D%80%2C%E7%A5%9E%E5%B2%A1%E5%8D%80%2C%E6%A2%A7%E6%A3%B2%E5%8D%80%2C%E6%B8%85%E6%B0%B4%E5%8D%80%2C%E6%96%B0%E7%A4%BE%E5%8D%80%2C%E6%BD%AD%E5%AD%90%E5%8D%80%2C%E9%BE%8D%E4%BA%95%E5%8D%80%2C%E8%B1%90%E5%8E%9F%E5%8D%80%2C%E9%9C%A7%E5%B3%B0%E5%8D%80&proptype=C51C52&saletype=1&keyword=&saledate1=&saledate2=&minprice1=&minprice2=&saleno=&crmyy=&crmid=&crmno=&dpt=&comm_yn=&stopitem=&sec=&rrange=&area1=&area2=&debtor=&checkyn=&emptyyn=&ttitle=&sorted_column=A.CRMYY%2C+A.CRMID%2C+A.CRMNO%2C+A.SALENO%2C+A.ROWID&sorted_type=ASC&_ORDER_BY=&pageNum=7&pageSize=50"
    headers = {
        'Content-Type':
        'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin':
        'https://aomp109.judicial.gov.tw',
        'Cookie':
        'TS018d5348=0126e2a9defef0d512dc65ce26000739199746e7ca8cac9e87a361fcd97d8f0647da7dfc99e25d71db60fc81ad7a0e91d31ca4f71e'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    keyList = redisKeyGet('internal')

    r = redis.StrictRedis(host='127.0.0.1', port=6379,
                          db=0, charset="utf-8", decode_responses=True)

    newCaseList = []
    changeCaseList = []

    for i in response.json()['data']:
        # check key exist or not
        tmpKey = i['rowid']  # -> int
        # keyList -> str
        if str(tmpKey) in keyList:
            defaultFalse = False
            defaultHashtable = {}
            for key, value in i.items():
                # check the value
                if str(value) != r.hget(tmpKey, key):
                    # update the value to DB
                    r.hset(tmpKey, key, value)
                    # save the value to dict(haskKey=tmpKey, key=value) do it tmr
                    if defaultFalse == False:
                        # first time to create the dict
                        # dict[rowId] = [(column, old value, newvalue)]
                        defaultHashtable[tmpKey] = [
                            (key, r.hget(tmpKey, key), str(value))]
                        defaultFalse = True
                    else:
                        # add tuple to list
                        tmpList = defaultHashtable[tmpKey]
                        tmpList.append((key, r.hget(tmpKey, key), str(value)))
                        # adjust dict list
                        defaultHashtable[tmpKey] = tmpList
            if defaultFalse:
                # means need to append dict to changeCaseList
                changeCaseList.append(defaultHashtable)
        else:
            # add to hash
            newCaseList.append(tmpKey)
            for key, value in i.items():
                r.hset(tmpKey, key, value)
    # write the result by result such as result_date
    curTime = datetime.datetime.now()
    curDate = curTime.strftime("%Y%m%d")
    # save result to json file
    with open(f"results_{curDate}.json", "w", encoding='utf-8') as fp:
        json.dump([newCaseList, changeCaseList], fp, ensure_ascii=False)
    fp.close()

    # save result to redis 

    return jsonify([newCaseList, changeCaseList])


@app.route('/getAllColumns/<hashKey>')
def redisGetAllColumns(hashKey):
    r = redis.StrictRedis(host='127.0.0.1', port=6379,
                          db=0, charset="utf-8", decode_responses=True)
    return jsonify(r.hkeys(hashKey))


@app.route('/getAllKeys')
def redisGetAllKeys():
    keyList = redisKeyGet('internal')
    print(len(keyList))
    return jsonify(keyList)

@app.route('/convertToJsonFile')
def redisCovertToJsonFile():
    keyList = redisKeyGet('internal')
    r = redis.StrictRedis(host='127.0.0.1', port=6379,db=0, charset="utf-8", decode_responses=True)
    curTime = datetime.datetime.now()
    curDate = curTime.strftime("%Y%m%d")
    resultContainer = []
    for index, hashKey in enumerate(keyList):
        resultContainer.append(r.hgetall(hashKey))

    with open(f"backup_{curDate}.json", "w", encoding='utf-8') as fp:
        json.dump(resultContainer, fp, ensure_ascii=False)
    fp.close()

    keysLength = len(resultContainer)

    return f'ok to backup, total keys - {keysLength}, fileName - backup_{curDate}.json'


@app.route('/backUpToRedis')
def backUpToRedis():
    keyList = redisKeyGet('internal')
    r = redis.StrictRedis(host='127.0.0.1', port=6379,db=0, charset="utf-8", decode_responses=True)
    curTime = datetime.datetime.now()
    curDate = curTime.strftime("%Y%m%d")
    resultContainer = []
    for hashKey in keyList:
        if len(hashKey) != 8:
            resultContainer.append(r.hgetall(hashKey))

    r.set(curDate, json.dumps(resultContainer))
    return 'ok'

@app.route('/getRedisString/<date>')
def getRedisString(date):
    r = redis.StrictRedis(host='127.0.0.1', port=6379,db=0, charset="utf-8", decode_responses=True)
    if r.get(date):
        resultList = json.loads(r.get(date))
    else:
        resultList = [f'does not backup for {date}']
    return jsonify(resultList)

@app.route('/analyze/change/<date>')
def redisAnalyze(date):
    # read json result file
    with open(f'results_{date}.json', 'r') as fobj:
            curList = json.load(fobj)[1]
    fobj.close()

    # connection Redis
    r = redis.StrictRedis(host='127.0.0.1', port=6379,db=0, charset="utf-8", decode_responses=True)

    returnList = []

    for i in curList:
        # print(list(i.values())[0])
        # returnList.append(r.hgetall(list(i.keys())[0]))
        rowId = list(i.keys())[0]
        if r.hget(rowId, 'c5x') == 'C51':
            tmpResultList = r.hmget(rowId, ['rowid','crtnm', 'crm', 'c5x', 'secstr', 'saleamtstr', 'area3str', 'saledate', 'salenostr', 'checkynstr', 'filenm'])
            tmpResultList[10] = 'https://aomp109.judicial.gov.tw/judbp/wkw/WHD1A02/DO_VIEWPDF.htm?filenm=' + tmpResultList[10]
        else:
            tmpResultList = r.hmget(rowId, ['rowid','crtnm', 'crm', 'c5x', 'budadd', 'saleamtstr', 'area3str', 'saledate', 'salenostr', 'checkynstr', 'filenm'])
            tmpResultList[10] = 'https://aomp109.judicial.gov.tw/judbp/wkw/WHD1A02/DO_VIEWPDF.htm?filenm=' + tmpResultList[10]
        returnList.append(tmpResultList)

    return render_template('new.html', list=returnList, changeList=curList) 
    # return jsonify(returnList)
    
# http://127.0.0.1:5000/analyze/new/20220112
@app.route('/analyze/new/<date>')
def redisAnalyzeNew(date):
    # read json result file
    with open(f'results_{date}.json', 'r') as fobj:
        curList = json.load(fobj)[0]
    fobj.close()

    # connection Redis
    r = redis.StrictRedis(host='127.0.0.1', port=6379,db=0, charset="utf-8", decode_responses=True)

    # reAppend result based on case type
    returnList = [] 

    # C51 土地, C52 房屋
    for i in curList:
        if r.hget(i, 'c5x') == 'C51':
            tmpResultList = r.hmget(i, ['rowid','crtnm', 'crm', 'c5x', 'secstr', 'saleamtstr', 'area3str', 'saledate', 'salenostr', 'checkynstr', 'filenm'])
            tmpResultList[10] = 'https://aomp109.judicial.gov.tw/judbp/wkw/WHD1A02/DO_VIEWPDF.htm?filenm=' + tmpResultList[10]
        else:
            tmpResultList = r.hmget(i, ['rowid','crtnm', 'crm', 'c5x', 'budadd', 'saleamtstr', 'area3str', 'saledate', 'salenostr', 'checkynstr', 'filenm'])
            tmpResultList[10] = 'https://aomp109.judicial.gov.tw/judbp/wkw/WHD1A02/DO_VIEWPDF.htm?filenm=' + tmpResultList[10]
        returnList.append(tmpResultList)

    return render_template('new.html', list=returnList) 