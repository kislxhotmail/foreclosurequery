import redis
import json

# connect redis
r = redis.StrictRedis(host='127.0.0.1', port=6379, db=0)

# load json file and covert to dict
with open('results.json', 'r') as fileObject:
    resultDict = json.loads(fileObject.read())

# list dict
for i in resultDict['data']:
    tmpKey = i['rowid']
    # dict
    for key, value in i.items():
        r.hset(tmpKey, key, value)

