import os


class Config:
    SECRET_KEY = os.urandom(16)
    # SQLALCHEMY_DATABASE_URI =
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_BINDS = {}


# class ProdConfig(Config):
#     DEBUG = False
class DevConfig(Config):
    DEBUG = True